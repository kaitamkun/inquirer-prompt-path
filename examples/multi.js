const path = require("path"),
	  inquirer = require("inquirer"),
	  { PathReference, exists } = require("../path-reference.js"),
	  { PathPrompt } = require("../path-prompt.js");

inquirer.prompt.registerPrompt("path", PathPrompt);

const questions = [{
	type: "path",
	name: "path",
	message: "Enter a path",
	default: process.cwd(),
	multi: true,
	validate: (answer) => exists(answer)? true : "The path does not exist",
	validateMulti: (answers) => answers.length > 0? true : "You must provide at least one path"
}];

inquirer.prompt(questions).then((result) => console.log("Chosen path:", result)).catch((err) => console.error("Error:", err));
