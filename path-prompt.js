const path = require("path"),
	inquirer = require("inquirer"),
	chalk = require("chalk"),
	readline = require("readline"),
	runAsync = require("run-async"),
	BasePrompt = require("inquirer/lib/prompts/base"),
	{ ShellPathAutocomplete } = require("./shell-path-autocomplete");

const TAB_KEY = "tab";
const ENTER_KEY = "return";
const RANGE_SIZE = 5;

// An {@link Inquirer} prompt for a single path. It supports auto completing paths similarly to zsh.
exports.PathPrompt = class PathPrompt extends BasePrompt {
	// Create a new prompt instance
	constructor(...args) {
		super(...args);
		const cwd = this.opt.cwd || this.opt.default || path.sep;

		this.opt = Object.assign({ filter: (value) => value, validate: () => true, validateMulti: () => true, when: () => true }, this.opt);
		this.cancelCount = 0;
		this.originalListeners = { SIGINT: this.rl.listeners("SIGINT") };
		this.listeners = [];
		this.answer = (this.opt.multi) ? [] : null;
		this.shell = new ShellPathAutocomplete(cwd || process.cwd(), this.opt.directoryOnly);
		this.opt.default = this.shell.getWorkingDirectory().getName();
		this.state = { selectionActive: false };

		this.onSubmit = this.onSubmit.bind(this);
		this.onCancel = this.onCancel.bind(this);
		this.onKeyPress = this.onKeyPress.bind(this);

		this.rl.removeAllListeners("SIGINT");
	};

	// Runs the prompt.
	// @param {function} cb - A callback to call once the prompt has been answered successfully
	// @returns {PathPrompt}
	_run(cb) {
		this.done = cb;
		this.rl.addListener("line", this.onSubmit);
		this.rl.addListener("SIGINT", this.onCancel);
		this.rl.input.addListener("keypress", this.onKeyPress);
		this.render();
		return this;
	};

	// Hanldles keyPress events.
	// @param {Object} value - The string representing the pressed key
	// @param {Object} key - The information about the key pressed
	onKeyPress(value, key = {}) {
		this.cancelCount = 0;
		switch (key.name) {
			case TAB_KEY:
				this.shell.refresh();
				if (this.shell.hasCommonPotentialPath()) {
					this.state.selectionActive = false;
					this.shell.setInputPath(this.shell.getCommonPotentialPath());
				} else if (!this.state.selectionActive) {
					this.state.selectionActive = true;
				} else {
					this.shell.selectNextPotentialPath(!key.shift);
				};

				this.resetCursor();
				break;
			case ENTER_KEY:
				if (!this.shell.hasSelectedPath()) return; // Let the onSubmit handler take care of that.
				this.shell.setInputPath(this.shell.getSelectedPath());
				this.state.selectionActive = false;
				this.resetCursor();
				break;
			default:
				this.state.selectionActive = false;
				this.shell.setInputPath(this.rl.line);
				break;
		};

		// Avoid polluting the line value with whatever new characters the action key added to the line
		this.rl.line = this.shell.getInputPath(true);
		this.render();
	};

	// Event handler for when the user submits the current input.
	// It is triggered when the enter key is pressed.
	onSubmit() {
		if (this.shell.hasSelectedPath()) {
			return; // Let the onKeypress handler take care of that one.
		};

		this.cancelCount = 0;
		const input = path.resolve(this.shell.getInputPathReference().getPath());
		runAsync(this.opt.validate, (err, isValid) => isValid === true? this.onSuccess(input) : this.onError(isValid))(input, this.answers);
	};

	// Event handler for cancel events (SIGINT)
	onCancel(...args) {
		if (this.shell.hasSelectedPath()) {
			this.state.selectionActive = false; // Cancel the path selection
			this.shell.resetSelectPotentialPath();
		} else if (this.opt.multi && this.cancelCount < 1) {
			runAsync(this.opt.validateMulti, (err, isValid) => {
				if (isValid === true) {
					this.onFinish();
				} else {
					this.cancelCount++;
					this.onError(isValid);
				};
			})(this.answer);
		} else {
			this.cleanup(); // Exit out
			this.originalListeners.SIGINT.forEach((listener) => listener(...args));
		};
	};

	// Handles validation errors.
	// @param {string} error - The validation error
	onError(error) {
		this.rl.line = this.shell.getInputPath(true); // Keep the state
		this.resetCursor();
		this.renderError(error);
	};

	// Handles a successful submission.
	// @param {string} value - The resolved input path
	onSuccess(value) {
		runAsync(this.opt.filter, (err, filteredValue) => { // Filter the value based on the options.
			this.render(filteredValue); // Re-render prompt with the final value
			if (this.opt.multi) {
				this.rl.output.unmute(); // Add a new line to keep the rendered answer
				this.rl.output.write("\n");
				this.rl.output.mute();
				this.shell.setInputPath(""); // Reset the shell
				this.shell.resetSelectPotentialPath();
				this.state = { selectionActive: false }; // Hide the selection if it was active
				this.answer.push(filteredValue);
				this.render(); // Render the new prompt
			} else {
				this.answer = filteredValue;
				this.onFinish();
			};
		})(value);
	};

	// Handles the finish event
	onFinish() {
		this.cleanup();
		this.screen.done();
		this.done(this.answer);
	};

	// Render the prompt
	render(finalAnswer) {
		this.screen.render(this.renderMessage(finalAnswer), finalAnswer ? "" : this.renderBottom());
	};

	// Render errors during the prompt
	// @param error
	renderError(error) {
		this.screen.render(this.renderMessage(), chalk.red(">> ") + error);
	};

	// Reset the input cursor to the end of the line
	resetCursor() {
		this.rl.output.unmute(); // Move the display cursor
		readline.cursorTo(this.rl.output, this.shell.getInputPath(true).length + 1);
		this.rl.output.mute();
		this.rl.cursor = this.shell.getInputPath(true).length; // Move the internal cursor
	};

	// Render the message part of the prompt. The message includes the question and the current response.
	// @returns {String}
	renderMessage(finalAnswer) {
		return this.getQuestion() + (finalAnswer? chalk.cyan(finalAnswer) : this.shell.getInputPath(true));
	};

	// Render the bottom part of the prompt. The bottom part contains all the possible paths.
	// @returns {string}
	renderBottom() {
		if (!this.state.selectionActive) return "";
		const potentialPaths = this.shell.getPotentialPaths(), selectedPath = this.shell.getSelectedPath(), selectedPathIndex = potentialPaths.indexOf(selectedPath);
		return this.slice(potentialPaths, selectedPathIndex, RANGE_SIZE).map((potentialPath) => potentialPath === selectedPath? chalk.black.bgWhite(potentialPath.getName() + (potentialPath.isDirectory()? path.sep : "")) : (potentialPath.isDirectory() ? chalk.red : chalk.green)(potentialPath.getName()) + (potentialPath.isDirectory() ? path.sep : "")).join("\n");
	};

	// Slice an array around a specific item so that it contains a specific number of elements.
	// @param {Array<T>} items - The array of items which should be shortened
	// @param itemIndex - The index of the item that should be included in the returned slice
	// @param size - The desired size of the array to be returned
	// @returns {Array<T>} An array with the number of elements specified by size.
	slice(items, itemIndex, size) {
		const length = items.length, min = itemIndex - Math.floor(size / 2), max = itemIndex + Math.ceil(size / 2);
		return items.slice(...(min < 0? [0, Math.min(length, max - min)] : (max >= length? [Math.max(0, min - (max - length)), length] : [min, max])));
	};

	// Unregister the local event handlers and reregister the ones that were removed.
	cleanup() {
		Object.keys(this.originalListeners).forEach((eventName) => {
			this.originalListeners[eventName].forEach((listener) => {
				this.rl.addListener(eventName, listener);
			});
		});

		this.rl.removeListener("line", this.onSubmit);
		this.rl.removeListener("SIGINT", this.onCancel);
		this.rl.input.removeListener("keypress", this.onKeyPress);
	};
};
